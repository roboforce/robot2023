package frc.robot;

public final class Constants {
    //readable wrigglers
    public static final int DRIVER_CONTROLLER_PORT = 2;
    public static final int DRIVER_JOYSTICK_PORT = 0;
    public static final int OPERATOR_JOYSTICK_PORT = 1;
    public static final int OPERATOR_CONTROLLER_PORT = 3;
    public static final int DANCE_DANCE_REVOLUTION_PORT = 4;

    //I DO NOT KNOW
    public static final int SOL_FORWARD_CHANNEL = 0;
    public static final int SOL_REVERSE_CHANNEL = 0;
    public static final int SOL2_FORWARD_CHANNEL = 0;
    public static final int SOL2_REVERSE_CHANNEL = 0;

    //buttons
    public static final int INTAKE_FORWARD_BUTTON = 2;
    public static final int INTAKE_BACKWARD_BUTTON = 3;

    public static final int ARM_ROTATE_UP_BUTTON = 0;
    public static final int ARM_ROTATE_DOWN_BUTTON = 0;

    public static final int ARM_EXTEND_BUTTON = 6;
    public static final int ARM_RETRACT_BUTTON = 4;

    public static final int ARM_TO_LEVEL_BUTTON = 2;
    public static final int ARM_TO_LEVEL_CONTROLLER_BTN = 0;

    public static final int ARM_EXTEND_WITH_CONTROLLER = 6;
    public static final int ARM_RETRACT_WITH_CONTROLLER = 5;

    public static final int CLAW_PINCH_BUTTON = 1;
    public static final int CLAW_PINCH_CONTROLLER_BUTTON = 3;

    public static final int INTAKE_ARM_UP_BUTTON = 1;
    public static final int INTAKE_ARM_DOWN_BUTTON = 1;

    public static final int CLAW_WHEELS_FWD_BUTTON = 10;
    public static final int CLAW_WHEELS_FWD_CONTROLLER = 0;

    public static final int CLAW_WHEELS_BWD_BUTTON = 11;
    public static final int CLAW_WHEELS_BWD_CONTROLLER = 0;

    //drivetrain
    public static final int DRIVETRAIN_LEFT_1_ID = 1;
    public static final int DRIVETRAIN_LEFT_2_ID = 2;

    public static final int DRIVETRAIN_RIGHT_1_ID = 3;
    public static final int DRIVETRAIN_RIGHT_2_ID = 4;

    //intake
    public static final int INTAKE_WHEEL_MOTOR_1_ID = 7;
    public static final int INTAKE_WHEEL_MOTOR_2_ID = 6;

    public static final int INTAKE_WHEEL_MOTOR_LEFT_ID = 13;
    public static final int INTAKE_WHEEL_MOTOR_RIGHT_ID = 14;

    public static final double INTAKE_WHEEL_MOTOR_H_SPEED = 0.625;
    public static final double INTAKE_WHEEL_MOTOR_L_SPEED = 0.625;
    public static final double INTAKE_WHEEL_MOTOR_SIDE_SPEED = 0.625;

    //intake arm
    public static final int INTAKE_ARM_MOTOR_ID = 5;

    public static final double INTAKE_ARM_KP = 0.03;
    public static final double INTAKE_ARM_KI = 0;
    public static final double INTAKE_ARM_KD = 0;


    //claw whels
    public static final double CLAW_WHEEL_SPEED = 0.80 ;
    public static final int CLAW_WHEEL_LEFT_MOTOR_ID = 0;
    public static final int CLAW_WHEEL_RIGHT_MOTOR_ID = 1;


    //indexer
    public static final int INDEX_ID_CAN = 12; // CAN ANYMORE
    public static final int INDEX_ID_PWM = 2;

    //arm arm
    public static final int LEFT_ARM_MOTOR_ID = 10;
    public static final int RIGHT_ARM_MOTOR_ID = 11;
    public static final int ARM_EXTENTION_ID = 9;
    
    public static final int ARM_ROTATING_SPEED = 0;
    public static final double ARM_ROTATE_PUSHBACK_SPEED = 0.07;
    public static final double ARM_EXTEND_PUSHBACK_SPEED = 0.03;

    public static final double ARM_ROTATE_HIGH = 245;
    public static final double ARM_EXTEND_HIGH = 24;

    //"milo is stupid"-COmputer
    //speeds or values
    public static final double INDEX_RUN_SPEED = 0.5;  
    public static final double ARM_RUN_SPEED = 0.30;


    //PIDsetpoints and stuff
    public static final int ARM_LOW_DEG = 0;
    public static final int ARM_LOW_POS = 0;

    public static final int ARM_MID_DEG = 0;
    public static final int ARM_MID_POS = 0;

    public static final int ARM_HIGH_DEG = 0;
    public static final int ARM_HIGH_POS = 1;

    //PID VALUES
    public static final int arm_kP = 0;
    public static final int arm_kI = 0;
    public static final int arm_kD = 0;

    public static final int rot_kP = 0;
    public static final int rot_kI = 0;
    public static final int rot_kD = 0;

    public static final int inchesToRotations = 0;
    
    public static final double angleForIndex = 31;
    public static final double extensionForIndex = 4.88;//5.27;


    public static final double INTAKE_ARM_UP_POS = -18;
    public static final double INTAKE_ARM_DOWN_POS = 0;

    //for kinematics and pose estimations
    public static final double ROBOT_TRACK_WIDTH = 0.6096;

    public static final double ANGLE_AUTO_HIGH = 256.5;//255
    public static final double EXTENSION_AUTO_HIGH = 29;

    public static final double ANGLE_FOR_MID = 266; //271
    public static final double EXTENSION_FOR_MID = 0;
    //255 ang high and 29 ext
    //273 ang low and 0 ext

}
