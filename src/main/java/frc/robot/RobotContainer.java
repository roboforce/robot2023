// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.armrotation.ArmRotateToLevel;
import frc.robot.commands.armrotation.ArmRotateToLevelNOFINISH;
import frc.robot.commands.armrotation.ArmRotationFollowJoystick;
import frc.robot.commands.autos.BlueOneConeLeftAuto;
import frc.robot.commands.autos.BlueOneConeRightAuto;
import frc.robot.commands.autos.GrabAndPlaceCone;
import frc.robot.commands.autos.OneConeLeftmostAuto;
import frc.robot.commands.autos.OneConeRightmostAuto;
import frc.robot.commands.autos.PlaceConeAndBalance;
import frc.robot.commands.autos.Taxi;
import frc.robot.commands.autos.TaxiLeft;
import frc.robot.commands.autos.WaitAndThen;
import frc.robot.commands.clawwheels.ClawWheelsBackward;
import frc.robot.commands.clawwheels.ClawWheelsForward;
import frc.robot.commands.clawwheels.ClawWheelsWithJoystick;
import frc.robot.commands.GrabConeFromIndexer;
import frc.robot.commands.armextension.ArmExtend;
import frc.robot.commands.armextension.ArmExtendToLevel;
import frc.robot.commands.armextension.ArmExtendToLevelNOFINISH;
import frc.robot.commands.armextension.ArmExtensionDoNothing;
import frc.robot.commands.armextension.ArmRetract;
import frc.robot.commands.drivetrain.DriveCharge;
import frc.robot.commands.drivetrain.DriveWithJoystick;
import frc.robot.commands.drivetrain.DrivetrainDoNothing;
import frc.robot.commands.drivetrain.DrivetrainTurnAngle;
import frc.robot.commands.indexer.IndexDoNothing;
import frc.robot.commands.indexer.IndexerWithJoystick;
import frc.robot.commands.intakearm.IntakeArmDoNothing;
import frc.robot.commands.intakearm.IntakeArmDown;
import frc.robot.commands.intakearm.IntakeArmUp;
import frc.robot.commands.intakewheels.IntakeWheelsDoNothing;
import frc.robot.commands.intakewheels.ReverseIntakeWheels;
import frc.robot.commands.intakewheels.RunIntakeWheels;
import frc.robot.commands.leds.LedsBlue;
import frc.robot.commands.leds.LedsColorz;
import frc.robot.commands.leds.LedsCycle;
import frc.robot.commands.leds.LedsOff;
import frc.robot.commands.leds.LedsPurple;
import frc.robot.commands.leds.LedsRandom;
import frc.robot.commands.leds.LedsRed;
import frc.robot.commands.leds.LedsWave;
import frc.robot.commands.leds.LedsYellow;
import frc.robot.subsystems.ArmExtension;
import frc.robot.subsystems.ArmRotation;
import frc.robot.subsystems.ClawWheels;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArm;
import frc.robot.subsystems.IntakeWheels;
import frc.robot.subsystems.LEDs;

public class RobotContainer {
  Drivetrain drivetrain = Drivetrain.getInstance();
  ArmRotation armRotation = ArmRotation.getInstance();
  ArmExtension armExtension = ArmExtension.getInstance();
  IntakeWheels intakeWheels = IntakeWheels.getInstance();
  IntakeArm intakeArm = IntakeArm.getInstance();
  Indexer indexer = Indexer.getInstance();
  LEDs leds = LEDs.getInstance();
  ClawWheels clawWheels = ClawWheels.getInstance();

  SendableChooser<Command> autoSelect = new SendableChooser<>();


  public RobotContainer() {
    drivetrain.setDefaultCommand(new DriveWithJoystick());
    intakeWheels.setDefaultCommand(new IntakeWheelsDoNothing());
    intakeArm.setDefaultCommand(new IntakeArmDoNothing());
    indexer.setDefaultCommand(new IndexerWithJoystick());
    armExtension.setDefaultCommand(new ArmExtensionDoNothing());
    armRotation.setDefaultCommand(new ArmRotationFollowJoystick());
    leds.setDefaultCommand(new LedsOff()); // its like rainbow but cooler since it has a z
    // claw.setDefaultCommand(new ClawPinch());
    clawWheels.setDefaultCommand(new ClawWheelsWithJoystick());

    autoSelect.addOption("just don't", new DrivetrainDoNothing());
    autoSelect.addOption("just grab the cone and score it", new GrabAndPlaceCone());
    autoSelect.addOption("*RED RIGHT one cone right red cube place low", new OneConeRightmostAuto());
    autoSelect.addOption("*RED LEFT one cone left red cube place low", new OneConeLeftmostAuto());
    autoSelect.addOption("Left Taxi", new TaxiLeft());
    autoSelect.addOption("Right Taxi", new Taxi());
    autoSelect.addOption("*BLUE RIGHT one cone right blue cube place low", new BlueOneConeRightAuto());
    autoSelect.addOption("*BLUE LEFT one cone grab cube place low", new BlueOneConeLeftAuto());
    autoSelect.addOption("DriveCharge?", new PlaceConeAndBalance());


    SmartDashboard.putData("AUTO", autoSelect);

    if (Robot.operatorUsingContoller()){
      configureControllerOperatorBindings();
    } else {
      configureJoystickOperatorBindings();
    }

    //configureControllerDriverBindings();

    if (Robot.driverUsingController()) {
      configureControllerDriverBindings();
    } else {
      configureJoystickDriverBindings();
    }

    configureDDRMatBindings();
  }

  private void configureControllerDriverBindings() {
    new JoystickButton(Robot.driverJoystick, 5)
      .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(107), new ArmExtendToLevelNOFINISH(7.46)));
    
    new JoystickButton(Robot.driverJoystick, 6)
      .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(99), new ArmExtendToLevel(7.46)));
  }

  private void configureJoystickDriverBindings() {
    // new JoystickButton(Robot.driverJoystick, 7)
    //   .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(107), new ArmExtendToLevelNOFINISH(7.46)));
    
    // new JoystickButton(Robot.driverJoystick, 6)
    //   .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(99), new ArmExtendToLevelNOFINISH(7.46)));

    new JoystickButton(Robot.driverJoystick, 6)
      .whileTrue(new LedsYellow());

    new JoystickButton(Robot.driverJoystick, 7)
      .whileTrue(new LedsPurple());

    new JoystickButton(Robot.driverJoystick, 11)
      .whileTrue(new LedsColorz());
  }

  private void configureJoystickOperatorBindings() {
    // new JoystickButton(Robot.operatorJoystick, 8)
    //   .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(258), new ArmExtendToLevelNOFINISH(27)));
    // new JoystickButton(Robot.operatorJoystick, 10)
    //   .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(273), new ArmExtendToLevelNOFINISH(0)));

    new JoystickButton(Robot.operatorJoystick, 8)
    .whileTrue(new ArmExtend());
    
    new JoystickButton(Robot.operatorJoystick, 10)
    .whileTrue(new ArmRetract());

    new JoystickButton(Robot.operatorJoystick, 1)
      .whileTrue(new ReverseIntakeWheels());
    new JoystickButton(Robot.operatorJoystick, 12)
      .whileTrue(new SequentialCommandGroup(new ArmExtendToLevel(-1.45), new ArmRotateToLevel(98.5)));
    // new JoystickButton(Robot.operatorJoystick, Constants.ARM_EXTEND_BUTTON)
    //   .whileTrue(new ArmExtend());
    new JoystickButton(Robot.operatorJoystick, 7)
      .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(246.0-5.0), new ArmExtendToLevelNOFINISH(29)));
    new JoystickButton(Robot.operatorJoystick, 9)
      .whileTrue(new SequentialCommandGroup(new ArmRotateToLevel(Constants.ANGLE_FOR_MID-2.0-7.5), new ArmExtendToLevel(Constants.EXTENSION_FOR_MID)));
    // new JoystickButton(Robot.operatorJoystick, Constants.ARM_RETRACT_BUTTON)
    //   .whileTrue(new ArmRetract());
     new JoystickButton(Robot.operatorJoystick, 2)
      .onTrue(new IntakeArmDown())
      .whileTrue(new RunIntakeWheels());

     new JoystickButton(Robot.operatorJoystick, 3)
      .onTrue(new IntakeArmUp());
    // new JoystickButton(Robot.operatorJoystick, 11)
    //   .whileTrue(new SequentialCommandGroup(new ArmExtendToLevel(-1.45), new ArmRotateToLevel(76.59)));

    new JoystickButton(Robot.operatorJoystick, 11)
      .whileTrue(new SequentialCommandGroup(new ArmExtendToLevel(-1.45), new ArmRotateToLevel(108))); // was 107

    new JoystickButton(Robot.operatorJoystick, 5)
      .whileTrue(new GrabConeFromIndexer());
  }

  private void configureControllerOperatorBindings() {

     new JoystickButton(Robot.operatorJoystick, Constants.ARM_TO_LEVEL_CONTROLLER_BTN)
         .onTrue(new ArmRotateToLevel(2))
         .onFalse(new ArmRotationFollowJoystick());

    new JoystickButton(Robot.operatorJoystick, Constants.CLAW_WHEELS_FWD_CONTROLLER)
      .whileTrue(new ClawWheelsForward());

    new JoystickButton(Robot.operatorJoystick, Constants.CLAW_WHEELS_BWD_CONTROLLER)
      .whileTrue(new ClawWheelsBackward());
    
    new JoystickButton(Robot.operatorJoystick, Constants.ARM_EXTEND_WITH_CONTROLLER)
      .whileTrue(new ArmExtend());
    
    new JoystickButton(Robot.operatorJoystick, Constants.ARM_RETRACT_WITH_CONTROLLER)
      .whileTrue(new ArmRetract());
    

    //dont use this you silly goober

    

  }

  private void configureDDRMatBindings() {
    new JoystickButton(Robot.danceDanceRevolution, 6) //square
      .whileTrue(new LedsYellow());

    new JoystickButton(Robot.danceDanceRevolution, 5) //triangler 
      .whileTrue(new LedsPurple());
    
    new JoystickButton(Robot.danceDanceRevolution, 2) // down 
      .whileTrue(new LedsColorz());

    new JoystickButton(Robot.danceDanceRevolution, 3) // up 
      .whileTrue(new LedsWave());
    
    new JoystickButton(Robot.danceDanceRevolution, 7) // up 
      .whileTrue(new LedsBlue());
    
    new JoystickButton(Robot.danceDanceRevolution, 10) // up 
      .whileTrue(new LedsRandom());

    new JoystickButton(Robot.danceDanceRevolution, 4) //right 
      .whileTrue(new LedsCycle(1));

    new JoystickButton(Robot.danceDanceRevolution, 1) //left
      .whileTrue(new LedsCycle(-1));

    new JoystickButton(Robot.danceDanceRevolution, 8)
      .whileTrue(new LedsRed());
      
  }

  public Command getAutonomousCommand() 
  {
    if (autoSelect.getSelected() != null) {
      return new WaitAndThen(autoSelect.getSelected(), SmartDashboard.getNumber("WaitTime", 0));
    }
    return new DrivetrainDoNothing();
  }
}
