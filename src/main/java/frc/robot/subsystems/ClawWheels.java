// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.VictorSP;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ClawWheels extends SubsystemBase {
  private static VictorSP leftWheelMotor = new VictorSP(Constants.CLAW_WHEEL_LEFT_MOTOR_ID);
  

  /** Creates a new ClawWheels. */
  public ClawWheels() { 
  }

  private static ClawWheels instance;
  public static ClawWheels getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch(InterruptedException e) {
        e.printStackTrace();
      }
      instance = new ClawWheels();
    }
    return instance;
  }

  
  public static void spinWheels(double speed) {
    leftWheelMotor.set(speed);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    
  }
}
