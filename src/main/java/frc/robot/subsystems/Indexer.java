// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Indexer extends SubsystemBase {
  public static PWMSparkMax indexMotor = new PWMSparkMax(Constants.INDEX_ID_PWM);

  /** Creates a new Indexer. */
  public Indexer() 
  {
    indexMotor.setInverted(false);
  }

  private static Indexer instance;
  public static Indexer getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      instance = new Indexer();
    }
    return instance;
  }

  public static void setSpeed(double speed)
  {
    indexMotor.set(speed);
  }

  public static void run ()
  {
    setSpeed(Constants.INDEX_RUN_SPEED);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
