// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IntakeWheels extends SubsystemBase {
  // private static CANSparkMax intakeMotorHigh = new CANSparkMax(Constants.INTAKE_WHEEL_MOTOR_1_ID, MotorType.kBrushless);
  private static CANSparkMax intakeMotorLow = new CANSparkMax(Constants.INTAKE_WHEEL_MOTOR_2_ID, MotorType.kBrushless);

  //private static CANSparkMax intakeLeftMotor = new CANSparkMax(Constants.INTAKE_WHEEL_MOTOR_LEFT_ID, MotorType.kBrushless);
  //private static CANSparkMax intakeRightMotor = new CANSparkMax(Constants.INTAKE_WHEEL_MOTOR_RIGHT_ID, MotorType.kBrushless);


  /** Creates a new IntakeWheels. */
  public IntakeWheels() {
    // intakeMotorHigh.restoreFactoryDefaults();
    intakeMotorLow.restoreFactoryDefaults();
    //intakeLeftMotor.restoreFactoryDefaults();
    //intakeRightMotor.restoreFactoryDefaults();
    
    //intakeLeftMotor.setSmartCurrentLimit(20);
    // intakeMotorHigh.setSmartCurrentLimit(20);
    intakeMotorLow.setSmartCurrentLimit(20);
    //intakeRightMotor.setSmartCurrentLimit(20);
    
    //intakeLeftMotor.setInverted(true);
  }

  private static IntakeWheels instance;
  public static IntakeWheels getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch(InterruptedException e) {
        e.printStackTrace();
      }
      instance = new IntakeWheels();
    }
    return instance;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public static void setSpeed(double highSpeed, double lowSpeed, double sideSpeed) {
    // intakeMotorHigh.set(highSpeed);
    intakeMotorLow.set(lowSpeed);
    //intakeLeftMotor.set(sideSpeed);
    //intakeRightMotor.set(sideSpeed);
  }
}
