// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.TalonFXInvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ArmRotation extends SubsystemBase {
  public static WPI_TalonFX leftMotor = new WPI_TalonFX(Constants.LEFT_ARM_MOTOR_ID);
  public static WPI_TalonFX rightMotor = new WPI_TalonFX(Constants.RIGHT_ARM_MOTOR_ID);


  private static final double ARM_ANGLE_SCALING_FACTOR = 1.0 / 4.0 / 5.0 / 64.0 * 18.0 / 2048.0 * 360.0 * (90.0/97.0);
  private static final double ARM_STARTING_ANGLE = 37.3; //degrees

  public static double output = 0;
  public static double setpointDeg = 0; //degrees

  /** Creates a new ArmRotation. */
  public ArmRotation() {
    leftMotor.configFactoryDefault();
    rightMotor.configFactoryDefault();

    leftMotor.follow(rightMotor);

    rightMotor.setInverted(false);
    leftMotor.setInverted(TalonFXInvertType.OpposeMaster);

    leftMotor.configOpenloopRamp(0);
    rightMotor.configOpenloopRamp(0);

    leftMotor.setNeutralMode(NeutralMode.Brake);
    rightMotor.setNeutralMode(NeutralMode.Brake);

    rightMotor.config_kP(0, 0.07);
    rightMotor.config_kD(0, 0.04);

    rightMotor.configClosedLoopPeakOutput(0, 0.35);
    rightMotor.setSelectedSensorPosition(ARM_STARTING_ANGLE);

  }

  private static ArmRotation instance;
  public static ArmRotation getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      instance = new ArmRotation();
    }
    return instance;
  }

  public static void setMotorKd(double kD){
    rightMotor.config_kD(0, kD);
  }

  public static void setMotorKp(double kP) {
    rightMotor.config_kP(0, kP);
  }
 
  public static void setRotatingSpeed(double speed)
  {
    // if (180 - Math.abs(getArmAngle()) < 45 && ArmExtension.getArmExtension() > 2 ) {
    //   rightMotor.set(0);
    //   return;
    // }
    
    double degrad = Math.toRadians(getArmAngle());

    double pushback = Math.sin(degrad) * Constants.ARM_ROTATE_PUSHBACK_SPEED;
    rightMotor.set(speed + pushback);
  }

  public static double getArmAngle() {
    double position = rightMotor.getSelectedSensorPosition();

    return position * ARM_ANGLE_SCALING_FACTOR + ARM_STARTING_ANGLE;
  } 

  public static double DegreesToEncoderTicks(double degrees) {
    return (degrees - ARM_STARTING_ANGLE)/ARM_ANGLE_SCALING_FACTOR;
  }

  public static void setRotationLevel(double inputPos) {
    // if (180 - Math.abs(getArmAngle()) < 45 && ArmExtension.getArmExtension() > 2) {
    //   rightMotor.set(0);
    //   return;
    // } 
    // output = inputPos;

    // if (inputPos == 2) {
    //   setpointDeg = 180;
    // } else if (inputPos == 7) {
    //   setpointDeg = Constants.ARM_MID_DEG;
    // } else if (inputPos == 8) {
    //   setpointDeg = Constants.ARM_LOW_DEG;
    // }

    // mid cone 245 angle 
    double degrad = Math.toRadians(getArmAngle());
    double pushback = Math.sin(degrad) * Constants.ARM_ROTATE_PUSHBACK_SPEED;
    rightMotor.set(ControlMode.Position, DegreesToEncoderTicks(inputPos), DemandType.ArbitraryFeedForward, pushback);
  }

  public static double getArmVelocity() {
    return rightMotor.getSelectedSensorVelocity();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("ANGLE", getArmAngle());
    SmartDashboard.putNumber("ROTVELOCITY", getArmVelocity());
  }
}
