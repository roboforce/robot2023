// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ArmExtension extends SubsystemBase {
  public static WPI_TalonFX extensionMotor = new WPI_TalonFX(Constants.ARM_EXTENTION_ID);

  public static double output = 0;
  public static double setpointPos = 0; //inches
  /** Creates a new ArmExtension. */
  public ArmExtension() {
    extensionMotor.configFactoryDefault();

    extensionMotor.config_kP(0, 0.09);
     extensionMotor.config_kD(0, 0.03);
    // extensionMotor.configClosedLoopPeakOutput(0, 0.25);
    extensionMotor.setNeutralMode(NeutralMode.Brake);
    extensionMotor.setInverted(true);
    extensionMotor.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 10, 12, 0.1));
  }
//Hi
//pen15
  private static ArmExtension instance;
  public static ArmExtension getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      instance = new ArmExtension();
    }
    return instance;
  }

  public static void setExtensionArmSpeed(double speed)
  {
    // if (180 - Math.abs(ArmRotation.getArmAngle()) < 45 && getArmExtension() > 2 ) {
    //   extensionMotor.set(-0.5);
    //   return;
    // } else if (180 - Math.abs(ArmRotation.getArmAngle()) < 45) {
    //   extensionMotor.set(Math.min(speed, 0));
    //   return;
    // }
    
    double degrad = Math.toRadians(ArmRotation.getArmAngle());

    double pushback = -Math.cos(degrad) * Constants.ARM_EXTEND_PUSHBACK_SPEED;

    extensionMotor.set(speed + pushback);
    SmartDashboard.putNumber("pushback", pushback);
  }

  public static void 
  setExtensionLevel(double sPointPos) {
    // if (180 - Math.abs(ArmRotation.getArmAngle()) < 45 && getArmExtension() > 2 ) {
    //   extensionMotor.set(-0.5);
    //   return;
    // } else if (180 - Math.abs(ArmRotation.getArmAngle()) < 45) {
    //   extensionMotor.set(Math.min(sPointPos, 0));
    //   return;
    // }
    // output = inputPos;
    // if (inputPos == 6) {
    //   setpointPos = Constants.ARM_HIGH_POS;
    // } else if (inputPos == 7) {
    //   setpointPos = Constants.ARM_MID_POS;
    // } else if (inputPos == 8) {
    //   setpointPos = Constants.ARM_LOW_POS;
    // }

    // 24 full extend 0 no extende
    // System.out.println(sPointPos + "," + extensionMotor.getEncoder().getPosition());
    extensionMotor.set(ControlMode.Position, (sPointPos + 1.6)*2048.0);
  }

  public static double getArmExtensionVelocity() {
    return extensionMotor.getSelectedSensorVelocity();
  }

  public static double getArmExtension() {
    return (extensionMotor.getSelectedSensorPosition() + 1.6)/2048.0;
  }

  public static double getArmExtensionError() {
    return extensionMotor.getClosedLoopError() / 2048.0;
  }
  static int overcurrent = 0;
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    // full out: 134.9744110107422, maybe -- BAD VALUE DONT USE OR DIE
    SmartDashboard.putNumber("Arm Extension", getArmExtension());
    SmartDashboard.putNumber("Arm Extension Speed", getArmExtensionVelocity());
    SmartDashboard.putNumber("extension current", extensionMotor.getSupplyCurrent());
    SmartDashboard.putNumber("ArM extension error", getArmExtensionError());
    

    // if (extensionMotor.getOutputCurrent() > 5.5) {
    //   overcurrent++;
    // } else {
    //   overcurrent = 0;
    // }
    // if (overcurrent > 15) {
    //   if (getArmExtension() > 12) {

    //   } else {
    //     extensionMotor.setSelectedSensorPosition(-0.7*2048);
    //   }
    // }
  }
}
