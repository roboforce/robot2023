// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj.ADIS16448_IMU;
import edu.wpi.first.wpilibj.ADIS16448_IMU.IMUAxis;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Drivetrain extends SubsystemBase {
  // Kinematics
  public static final double wheelCircumferenceInMeters = 0.47877;
  public static final double kTrackWidthMeters = 0.69557;
  public static final double kMaxSpeedMetersPerSecond = 3;
  public static final double kMaxAccelerationPerSecond = 0.5;
  public static final double rotationsToMeters  = (1/9.822222222222) * wheelCircumferenceInMeters;

  // SysId output
  public static final double DRIVETRAIN_KS = 0.14882;
  public static final double DRIVETRAIN_KV = 2.5887;
  public static final double DRIVETRAIN_KA = 0.72593;
  public static final double DRIVETRAIN_kP_WPILIB = 1.6992/10.0; // 0.088093
  public static final double DRIVETRAIN_kP_CTRE = 2.3667/10.0;  // 0.081104
  public static final double DRIVETRAIN_RES_TIMESCALE = 280.42;

  public static final double kRamseteB = 2;
  public static final double kRamseteZeta = 0.7;

  public static final double DRIVETRAIN_POS_MEASURE_DELAY = 12.446;
  public static final double DRIVETRAIN_VEL_MEASURE_DELAY = 451.22;

  public static final double DRIVETRAIN_WINDOW_SIZE = 1;

  public static final double DRIVETRAIN_VEL_THRESHOLD = 0.014; // 0.357

  public static final double TURNING_MULTIPLIER = 1.0;

  // moters
  public static CANSparkMax leftMotor1 = new CANSparkMax(Constants.DRIVETRAIN_LEFT_1_ID, MotorType.kBrushless);
  public static CANSparkMax leftMotor2 = new CANSparkMax(Constants.DRIVETRAIN_LEFT_2_ID, MotorType.kBrushless);

  public static CANSparkMax rightMotor1 = new CANSparkMax(Constants.DRIVETRAIN_RIGHT_1_ID, MotorType.kBrushless);
  public static CANSparkMax rightMotor2 = new CANSparkMax(Constants.DRIVETRAIN_RIGHT_2_ID, MotorType.kBrushless);

  public static DifferentialDrive drivetrain = new DifferentialDrive(leftMotor1, rightMotor1);

  public static ADIS16448_IMU gyro = new ADIS16448_IMU();

  public static DifferentialDriveKinematics kDriveKinematics = new DifferentialDriveKinematics(kTrackWidthMeters);

  public static DifferentialDriveVoltageConstraint autoVoltageConstraint = new DifferentialDriveVoltageConstraint(new SimpleMotorFeedforward(DRIVETRAIN_KS, DRIVETRAIN_KV, DRIVETRAIN_KA), kDriveKinematics, 5);

  public static TrajectoryConfig tConfig = new TrajectoryConfig(kMaxSpeedMetersPerSecond, kMaxAccelerationPerSecond).setKinematics(kDriveKinematics).addConstraint(autoVoltageConstraint);


  public static DifferentialDriveOdometry odometry;

  public static double getLeftMotorDistance()
  {
    return leftMotor1.getEncoder().getPosition()*rotationsToMeters;
  }

  public static double getRightMotorDistance()
  {
    return rightMotor1.getEncoder().getPosition()*rotationsToMeters;
  }

  public static double getGyroAngle() {
    return gyro.getAngle();
  }

  public static double getGyroTilt() {
    return gyro.getGyroAngleX();
  }

  /** Creates a new Drivetrain. */
  public Drivetrain() {
    
    leftMotor1.restoreFactoryDefaults();
    leftMotor2.restoreFactoryDefaults();

    rightMotor1.restoreFactoryDefaults();
    rightMotor2.restoreFactoryDefaults();

    leftMotor1.setInverted(true);

    leftMotor2.follow(leftMotor1);
    
    rightMotor2.follow(rightMotor1);

    leftMotor1.setOpenLoopRampRate(.2);
    rightMotor1.setOpenLoopRampRate(.2);

    leftMotor1.getEncoder().setVelocityConversionFactor(rotationsToMeters); //uhhh i think so!?????
    rightMotor1.getEncoder().setVelocityConversionFactor(rotationsToMeters);

    leftMotor1.getEncoder().setPositionConversionFactor(1); // porbably wont do a thing but worth a shot i guess
    rightMotor1.getEncoder().setPositionConversionFactor(1);

    gyro.setYawAxis(IMUAxis.kZ);


    resetEncoders();
    resetGyro();

    odometry = new DifferentialDriveOdometry(gyroAsRot2d(), getLeftMotorDistance(), getRightMotorDistance(), new Pose2d(new Translation2d(), gyroAsRot2d()));
    
    brakeMode();
  }

  public static Pose2d getPose()
  {
    return odometry.getPoseMeters();
  }

  public static void resetGyro() {
    gyro.reset();
  }

  public static Rotation2d gyroAsRot2d() {
    return new Rotation2d(Math.toRadians(-gyro.getAngle()));
  }

  public static void resetEncoders() {
    leftMotor1.getEncoder().setPosition(0);
    rightMotor1.getEncoder().setPosition(0);
  }

  public static void enableOpenLoopRate() {
    leftMotor1.setOpenLoopRampRate(.2);
    rightMotor1.setOpenLoopRampRate(.2);
  }

  public static void disableOpenLoopRate() {
    leftMotor1.setOpenLoopRampRate(0);
    rightMotor1.setOpenLoopRampRate(0);
  }

  public static void resetOdometry(Pose2d pose)
  {
    // resetEncoders();
    // resetGyro();
    odometry.resetPosition(gyroAsRot2d(), getLeftMotorDistance(), getRightMotorDistance(), pose);
  }

  public static void resetOdometry()
  {
    resetOdometry(new Pose2d(new Translation2d(), new Rotation2d(0)));
    System.out.println("***********************************************RESETED/n*********RESETESTSAAAA");
  }

  public static void tankDriveVolts(double left, double right)
  {
    double diff = left - right;

    double leftAdjusted = left + (diff/2) * TURNING_MULTIPLIER;
    double rightAdjusted = right - (diff/2) * TURNING_MULTIPLIER;
    
    leftMotor1.setVoltage(leftAdjusted);
    rightMotor1.setVoltage(rightAdjusted);
    drivetrain.feed();
    SmartDashboard.putNumber("voltageLeft" , left);
    SmartDashboard.putNumber("voltageRight", right);
  }

  private static Drivetrain instance; 
  public static Drivetrain getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      instance = new Drivetrain();
    }
    return instance;
  }

  public static void brakeMode() {
    leftMotor1.setIdleMode(IdleMode.kBrake);
    leftMotor2.setIdleMode(IdleMode.kBrake);

    rightMotor1.setIdleMode(IdleMode.kBrake);
    rightMotor2.setIdleMode(IdleMode.kBrake);
  }

  public static void coastMode() 
  {
    leftMotor1.setIdleMode(IdleMode.kCoast);
    leftMotor2.setIdleMode(IdleMode.kCoast);
    
    rightMotor1.setIdleMode(IdleMode.kCoast);
    rightMotor2.setIdleMode(IdleMode.kCoast);
  }

  public static void arcadeDrive(double speed, double rotation) {
    drivetrain.arcadeDrive(speed, rotation);
  }

  public static DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(leftMotor1.getEncoder().getVelocity()/60, rightMotor1.getEncoder().getVelocity()/60);
  }    Field2d field = new Field2d();

  public static void driveDistance (double distance)
  {
    double setpoint = distance;
    leftMotor1.getPIDController().setP(0.12);
    leftMotor1.getPIDController().setD(0.04);
    rightMotor1.getPIDController().setP(0.12);
    rightMotor1.getPIDController().setD(0.04);
    leftMotor1.getPIDController().setOutputRange(-0.5, 0.5);
    rightMotor1.getPIDController().setOutputRange(-0.5, 0.5);
    leftMotor1.getPIDController().setReference(setpoint/rotationsToMeters, ControlType.kPosition);
    rightMotor1.getPIDController().setReference(setpoint/rotationsToMeters, ControlType.kPosition);
    drivetrain.feed();
  }

  @Override
  public void periodic() { 
    //odometry
    // System.out.println(getLeftMotorDistance());
    // System.out.println(leftMotor1.getEncoder());
    // System.out.println(leftMotor1.getEncoder().getPosition());
    // System.out.println(leftMotor1.getEncoder().getPosition()*rotationsToMeters);
    odometry.update(gyroAsRot2d(), getLeftMotorDistance(), getRightMotorDistance());


    //smart dashboard debug
    field.setRobotPose(getPose());
    SmartDashboard.putData("FIELD", field);
    SmartDashboard.putNumber("LM1", getLeftMotorDistance());
    SmartDashboard.putNumber("RM1", getRightMotorDistance());
    SmartDashboard.putNumber("GYRO", -gyro.getAngle());
    SmartDashboard.putNumber("GYROTEMPCELSIUS", gyro.getTemperature());
    SmartDashboard.putNumber("left speed:", leftMotor1.getEncoder().getVelocity()/60);
    SmartDashboard.putNumber("right speed:", rightMotor1.getEncoder().getVelocity()/60);
    SmartDashboard.putNumber("Tilt", getGyroTilt());
  }
  
}
