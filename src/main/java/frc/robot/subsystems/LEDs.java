// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class LEDs extends SubsystemBase {
  private static final int LED_STRIP_LEN = 144;
  static AddressableLED led = new AddressableLED(9);
  static AddressableLEDBuffer buffer = new AddressableLEDBuffer(LED_STRIP_LEN);

  private static final int[] hueList = {
    0,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    100,
    110,
    120,
    130,
    140,
    150,
    160,
    170,
  };

  private static int pos = 0;

  /** Creates a new LEDs. */
  public LEDs() {
    led.setLength(LED_STRIP_LEN);
    //clearLed();
    led.start();
  }

  public static void clearLed() {
    for (int i = 0; i < LED_STRIP_LEN; i++) {
      buffer.setHSV(i, 0, 0, 0);
    }
    led.setData(buffer);
    //led.stop();
  }

  public static void setPurple() {
    //led.start();
    for (int i = 0; i < LED_STRIP_LEN; i++) {
      buffer.setHSV(i, 160, 255, 255);
    }

    led.setData(buffer);
  }

  public static void setYellow() {
    //led.start();
    for (int i = 0; i < buffer.getLength(); i++) {
      buffer.setHSV(i, 20, 255, 255);
    }

    led.setData(buffer);
  }

  public static void setRed() {
    for (int i = 0; i < buffer.getLength(); i++) {
      buffer.setHSV(i, 0, 255, 255);
    }

    led.setData(buffer);
  }

  public static void setColorz() {
    //led.start();
    for (int i = 0; i < 144; i++) {
      buffer.setHSV(i, (i+pos)%180, 255, 100);
    }

    // for (int i = 0; i < 144; i++) {
    //   int color = 143 - i;
    //   buffer.setHSV(i + 144, (color+pos)%180, 255, 100);
    // }

    pos += 2;

    led.setData(buffer);
  }

  public static void setCycle(int index) {
    while (index < 0) {
      index += hueList.length;
    }

    for (int i = 0; i < 144; i++) {
      buffer.setHSV(i, hueList[index%hueList.length], 255, 255);
    }

    led.setData(buffer);
  }

  public static void setWave() {
    for (int i = 0; i < 144; i++) {
      int value = (int) (Math.sin((((double)(i+pos))*0.2))*90);
      value += 90;
      buffer.setHSV(i, value, 255, 100);
    }
    
    pos += 1;

    led.setData(buffer);
  }

  public static void setBlue() {
    //led.start();
    for (int i = 0; i < buffer.getLength(); i++) {
      buffer.setHSV(i, 120, 255, 255);
    }

    led.setData(buffer);
  }

  public static void setRandom() {
    //led.start();
    double dub = Math.random() * 180;
    int hue = (int) dub;
    for (int i = 0; i < buffer.getLength(); i++) {
      buffer.setHSV(i, hue, 255, 255);
    }

    led.setData(buffer);
  }

  private static LEDs instance;
  public static LEDs getInstance() {
    if (instance == null)  {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      instance = new LEDs();
    }
    return instance;
  }

  @Override
  public void periodic() {
    //LEDs.setPurple();
    // LEDs.setColorz();
  }
}
