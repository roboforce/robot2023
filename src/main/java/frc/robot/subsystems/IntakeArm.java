// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IntakeArm extends SubsystemBase {
  public static CANSparkMax armMotor = new CANSparkMax(Constants.INTAKE_ARM_MOTOR_ID, MotorType.kBrushless);
  public static boolean isDown = false;

  /** Creates a new IntakeArm. */
  public IntakeArm() {
    armMotor.restoreFactoryDefaults();

    //armMotor.getEncoder().setPositionConversionFactor(1);
    var pid = armMotor.getPIDController();

    pid.setP(Constants.INTAKE_ARM_KP);
    pid.setI(Constants.INTAKE_ARM_KI);
    pid.setD(Constants.INTAKE_ARM_KD);

    armMotor.setIdleMode(IdleMode.kCoast);
  }

  private static IntakeArm instance; 
  public static IntakeArm getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch(InterruptedException e) {
        e.printStackTrace();
      }
      instance = new IntakeArm();
    }
    return instance;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    //up: -18.928510665893555 like -18 should work
    //down: -0.047618940472603 like 0 or maybe 0.05 to be spicy
    SmartDashboard.putNumber("INTAKEARMPOS", armMotor.getEncoder().getPosition());
    SmartDashboard.putBoolean("INTAKEDOWN", isDown);
  }

  public static void setSpeed(double speed) {
    armMotor.set(speed);
  }

  //goofy ahh code tbh
  public static void setMotorReference(double pos) {
    armMotor.getPIDController().setReference(pos, CANSparkMax.ControlType.kPosition);
  }

  public static double getMotorPosition()
  {
    return armMotor.getEncoder().getPosition();
  }
}
