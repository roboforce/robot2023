// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.intakewheels;

import java.lang.invoke.ConstantBootstraps;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.IntakeWheels;

public class ReverseLowerIntakeWheels extends CommandBase {
  /** Creates a new ReverseLowerIntakeWheels. */
  public ReverseLowerIntakeWheels() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(IntakeWheels.getInstance());
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    IntakeWheels.setSpeed(-Constants.INTAKE_WHEEL_MOTOR_L_SPEED, 0, 0);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    IntakeWheels.setSpeed(0, 0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
