// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.intakewheels;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.IntakeWheels;

public class RunIntakeWheels extends CommandBase {
  /** Creates a new RunIntakeWheels. */
  public RunIntakeWheels() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(IntakeWheels.getInstance());
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    IntakeWheels.setSpeed(Constants.INTAKE_WHEEL_MOTOR_H_SPEED, Constants.INTAKE_WHEEL_MOTOR_L_SPEED, Constants.INTAKE_WHEEL_MOTOR_SIDE_SPEED);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
