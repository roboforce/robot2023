// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.intakearm;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.IntakeArm;

public class IntakeArmUp extends CommandBase {
  /** Creates a new IntakeArmUp. */
  public IntakeArmUp() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(IntakeArm.getInstance());
    IntakeArm.isDown = false;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    //IntakeArm.setMotorReference(Constants.INTAKE_ARM_UP_POS);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
   IntakeArm.setSpeed(-0.3);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    IntakeArm.setSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (IntakeArm.getMotorPosition() < 2);
  }
}
