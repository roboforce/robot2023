// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autos;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.GrabConeFromIndexer;
import frc.robot.commands.armextension.ArmExtendToLevel;
import frc.robot.commands.armextension.ArmExtendToLevelNOFINISH;
import frc.robot.commands.armrotation.ArmRotateToLevel;
import frc.robot.commands.clawwheels.ClawWheelsAtSpeed;
import frc.robot.commands.clawwheels.ClawWheelsBackward;
import frc.robot.commands.clawwheels.ClawWheelsDoNothing;
import frc.robot.commands.clawwheels.ClawWheelsForward;
import frc.robot.commands.intakearm.IntakeArmDown;
import frc.robot.commands.intakearm.IntakeArmUp;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class GrabAndPlaceConeNoRetract extends SequentialCommandGroup {
  /** Creates a new GrabAndPlaceCone. */
  public GrabAndPlaceConeNoRetract() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new GrabConeFromIndexer(),
      new ArmRotateToLevel(Constants.ANGLE_AUTO_HIGH -18.5),
      new ArmExtendToLevel(29),
      new ArmRotateToLevel(Constants.ANGLE_AUTO_HIGH),
      new WaitCommand(0.1),
      new ParallelRaceGroup(
        new ClawWheelsAtSpeed(Constants.CLAW_WHEEL_SPEED/3.0),
        new WaitCommand(1)
      ),
      new ClawWheelsDoNothing()
    );
  }
}
