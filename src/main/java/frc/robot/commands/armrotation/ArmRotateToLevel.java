// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.armrotation;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmRotation;

public class ArmRotateToLevel extends CommandBase {
  /** Creates a new ArmRotateToLevel. */
  public double angle;

  /**
   * @param angle The goal level. NOT THE BUTTON.
   */
  public ArmRotateToLevel(double angle) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ArmRotation.getInstance());
    this.angle = angle;
    
    // ArmRotation.setMotorKp(0.07);
    // ArmRotation.setMotorKd(0.04);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() 
  {
    ArmRotation.setRotationLevel(angle);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (Math.abs(ArmRotation.getArmAngle() - angle) < 3.5) && (Math.abs(ArmRotation.getArmVelocity()) < 250);
  }
}
