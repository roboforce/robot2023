// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.armrotation;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.ArmRotation;

public class ArmRotationFollowJoystick extends CommandBase {
  /** Creates a new ArmRotationFollowJoystick. */
  public ArmRotationFollowJoystick() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ArmRotation.getInstance());
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (Robot.operatorUsingContoller()) {
      ArmRotation.setRotatingSpeed((Robot.operatorJoystick.getRawAxis(3) - Robot.operatorJoystick.getRawAxis(2)) * 0.7);
    } else {
      double rate = Robot.operatorJoystick.getRawAxis(1);
      ArmRotation.setRotatingSpeed(Math.signum(rate) * rate * rate);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
