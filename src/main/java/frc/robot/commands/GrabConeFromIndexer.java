// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.armextension.ArmExtendToLevel;
import frc.robot.commands.armextension.ArmExtensionStayIn;
import frc.robot.commands.armrotation.ArmRotateToLevel;
import frc.robot.commands.clawwheels.ClawWheelsBackward;
import frc.robot.commands.clawwheels.ClawWheelsDoNothing;
import frc.robot.commands.clawwheels.ClawWheelsForward;
import frc.robot.commands.intakearm.IntakeArmDown;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class GrabConeFromIndexer extends SequentialCommandGroup {
  /** Creates a new GrabConeFromIndexer. */
  public GrabConeFromIndexer() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      // new IntakeArmDown(),
      // new ArmRotateToLevel(Constants.angleForIndex),
      // new ArmExtendToLevel(-1.2),
      // new ParallelRaceGroup(
      //   new ArmExtendToLevel(Constants.extensionForIndex),
      //   new ClawWheelsBackward()
      // ),
      // new ParallelRaceGroup(
      //   new ClawWheelsBackward(),
      //   new WaitCommand(0.5)
      // )
    
      new ParallelRaceGroup(
        new ClawWheelsBackward(),
        new WaitCommand(0.5)
      )
    );
  }
}
