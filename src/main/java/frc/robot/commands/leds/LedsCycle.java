// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.leds;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LEDs;

public class LedsCycle extends CommandBase {
  private static int index = 0;
  public int moveBy = 0;
  /** Creates a new LedsCycle. */
  public LedsCycle(int a_moveBy) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(LEDs.getInstance());
    moveBy = a_moveBy;
  }

  @Override
  public boolean runsWhenDisabled() {
    return true;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("a;ohefugak;uf;kuhfo;iae;flyaleifhlaiehf;lahe;ofhel;fhaw;elifh;aliehf;laiehf;laiehf;liaehf;liawhe;fileh");
    index += moveBy;
    LEDs.setCycle(index);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
