// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.clawwheels;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClawWheels;

public class ClawWheelsAtSpeed extends CommandBase {
  /** Creates a new ClawWheelsAtSpeed. */
  private double speed = 0;
  public ClawWheelsAtSpeed(double speed) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ClawWheels.getInstance());
    this.speed = speed;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    ClawWheels.spinWheels(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
