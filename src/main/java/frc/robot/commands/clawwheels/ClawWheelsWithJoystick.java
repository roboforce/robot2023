// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.clawwheels;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.subsystems.ClawWheels;

public class ClawWheelsWithJoystick extends CommandBase {
  /** Creates a new ClawWithJoystick. */
  public ClawWheelsWithJoystick() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ClawWheels.getInstance());
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double pov = Robot.operatorJoystick.getPOV();
    double speed = -0.15;
    if(pov > 90 && pov < 270) {
      speed = -Constants.CLAW_WHEEL_SPEED;
    } else if (pov > 270 || (pov > -1 && pov < 90)) {
      speed = Constants.CLAW_WHEEL_SPEED;
    }
    ClawWheels.spinWheels(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
