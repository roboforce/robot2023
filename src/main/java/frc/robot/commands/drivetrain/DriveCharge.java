// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.armextension.ArmExtendToLevel;
import frc.robot.commands.armrotation.ArmRotateToLevel;
import frc.robot.commands.intakearm.IntakeArmUp;
import frc.robot.commands.intakewheels.ReverseIntakeWheels;
import frc.robot.commands.intakewheels.ReverseLowerIntakeWheels;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveCharge extends SequentialCommandGroup {
  /** Creates a new DriveCharge. */
  public DriveCharge() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new IntakeArmUp(),
      new ParallelRaceGroup(
        new DriveForwardUntilAngle(9.527192873876),
        new ReverseLowerIntakeWheels(),
        new WaitCommand(4.5)
      ),  
      new ArmExtendToLevel(0),
      new ArmRotateToLevel(90),
      new DriveDistanceInMeters(1.19)
    ); 
  }
}
