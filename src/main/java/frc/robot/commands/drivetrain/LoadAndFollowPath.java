// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import java.io.IOException;
import java.nio.file.Path;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import frc.robot.subsystems.Drivetrain;

public class LoadAndFollowPath extends CommandBase {
  /** Creates a new LoadAndFollowPath. */
  private RamseteCommand ramseteCommand;
  private Trajectory trajectory;

  private boolean resetOdometry;

  public LoadAndFollowPath(String filename, boolean resetOdometry) {
    addRequirements(Drivetrain.getInstance());
    filename = "paths/output/" + filename + ".wpilib.json";
    try {
      Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(filename);
      //THIS IS DANGEROUS
      trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);

      this.resetOdometry = resetOdometry;

      ramseteCommand = new RamseteCommand(
        trajectory, 
        Drivetrain::getPose, 
        new RamseteController(Drivetrain.kRamseteB, Drivetrain.kRamseteZeta), 
        new SimpleMotorFeedforward(Drivetrain.DRIVETRAIN_KS, Drivetrain.DRIVETRAIN_KV, Drivetrain.DRIVETRAIN_KA), 
        Drivetrain.kDriveKinematics, 
        Drivetrain::getWheelSpeeds, 
        new PIDController(2, 0, 0), 
        new PIDController(2, 0, 0), 
        Drivetrain::tankDriveVolts, 
        Drivetrain.getInstance()
      );
    } catch (IOException e) {
      DriverStation.reportError("your path sucks broski (" + filename + ")", e.getStackTrace());
    }
    // Use addRequirements() here to declare subsystem dependencies.
  }

  public LoadAndFollowPath(String filename) {
    this(filename, true);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (resetOdometry){
      Drivetrain.resetOdometry(trajectory.getInitialPose());
    }
    Drivetrain.disableOpenLoopRate();
    ramseteCommand.initialize();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    ramseteCommand.execute();
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    ramseteCommand.end(interrupted);
    Drivetrain.enableOpenLoopRate();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return ramseteCommand.isFinished();
  }
}
