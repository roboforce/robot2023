// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class DriveForwardUntilAngle extends CommandBase {
  /** Creates a new DriveForwardUntilAngle. */
  private double goalAngle = 0;
  //previous was 0.469
  private static final double DRIVE_SPEED = 0.469-0.2;
  public DriveForwardUntilAngle(double angle) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(Drivetrain.getInstance());
    this.goalAngle = angle;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    Drivetrain.resetGyro();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    Drivetrain.arcadeDrive(DRIVE_SPEED, 0);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (Math.abs(Drivetrain.getGyroTilt()) >= goalAngle);
  }
}
