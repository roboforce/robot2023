// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class DriveDiagonalUntilAngle extends LoadAndFollowPath {
  private double goalAngle;

  private static final double DRIVE_SPEED = 0.325;

  /** Creates a new DriveUntilAngle. */
  public DriveDiagonalUntilAngle(double angle)  {
    // Use addRequirements() here to declare subsystem dependencies.
    super("Testing");
    goalAngle = angle;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    Drivetrain.resetGyro();
    super.initialize();
  }



  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (Math.abs(Drivetrain.getGyroTilt()) >= goalAngle) || super.isFinished();
  }
}
