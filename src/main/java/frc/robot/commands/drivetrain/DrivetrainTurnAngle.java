// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class DrivetrainTurnAngle extends CommandBase {
  PIDController rotController = new PIDController(0.027, 0, 0);
  double angle;
  double setPoint;

  /** Creates a new DrivetrainTurnAngle. */
  public DrivetrainTurnAngle(double angle) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(Drivetrain.getInstance());
    this.angle = angle;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    setPoint = -Drivetrain.getGyroAngle() + angle;
    rotController.setSetpoint(setPoint);
    rotController.reset();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    double rotAmount = rotController.calculate(-Drivetrain.getGyroAngle());
    rotAmount = Math.min(rotAmount, 0.65);
    rotAmount = Math.max(rotAmount, -0.65);
    Drivetrain.arcadeDrive(0, rotAmount);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}
//WORK PLS :)
  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Math.abs(rotController.getPositionError()) < 4.0;
  }
}
