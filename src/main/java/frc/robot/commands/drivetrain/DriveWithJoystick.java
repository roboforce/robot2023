// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.Drivetrain;

public class DriveWithJoystick extends CommandBase {
  /** Creates a new DriveWithJoystick. */
  public DriveWithJoystick() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(Drivetrain.getInstance());
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    Drivetrain.enableOpenLoopRate();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double rot = -Robot.driverJoystick.getRawAxis(4);
    rot = 0.75*(Math.signum(rot) * Math.sqrt(Math.abs(rot)));
    
    if (Robot.driverUsingController()) {
      Drivetrain.arcadeDrive(-Robot.driverJoystick.getRawAxis(1)*0.85, rot*0.85); // DONT FORGET TO TEST THIS MILO PLEASE PLEASE PLEASE PLEASE
    } else {
      Drivetrain.arcadeDrive(-Robot.driverJoystick.getRawAxis(1), -Robot.driverJoystick.getRawAxis(0));
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
