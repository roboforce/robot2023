// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.armextension;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmExtension;

public class ArmExtendToLevel extends CommandBase {
  /** Creates a new ArmExtendToLevel. */
  private double level = 0;
  public ArmExtendToLevel(double input) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ArmExtension.getInstance());
    level = input;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() 
  {
    ArmExtension.setExtensionLevel(level);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
      return Math.abs(ArmExtension.getArmExtension() - level) < 2.5 && ArmExtension.getArmExtensionVelocity() < 100.0;
  }
}
