// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.armextension;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ArmExtension;

public class ArmExtend extends CommandBase {
  /** Creates a new ArmExtend. */
  public ArmExtend() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ArmExtension.getInstance());

  }
  public int i = 0;

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    i = 0;

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // ArmExtension.setExtensionArmSpeed(Constants.ARM_RUN_SPEED);
    // i++;
    // if (i < 4)
    // {
    //   ArmExtension.setExtensionArmSpeed(1);
    // }
    // else 
    // {
    //   ArmExtension.setExtensionArmSpeed(Constants.ARM_RUN_SPEED);
    // }
    
    ArmExtension.setExtensionArmSpeed(Constants.ARM_RUN_SPEED);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
