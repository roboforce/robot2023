// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.indexer;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.Indexer;

public class IndexerWithJoystick extends CommandBase {
  /** Creates a new IndexerWithJoystick. */
  public IndexerWithJoystick() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(Indexer.getInstance());
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double value = 0;
    if (Robot.operatorUsingContoller()) {

    } else {
      value = ((Joystick)Robot.operatorJoystick).getX(); // i think this is right????
      value = Math.signum(value) * value * value;
    }

    Indexer.setSpeed(value);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
