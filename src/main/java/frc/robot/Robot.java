// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
public class Robot extends TimedRobot {
  public static boolean driverUsingController() {
    return DriverStation.getJoystickIsXbox(0);
  }

  public static boolean operatorUsingContoller() {
    return DriverStation.getJoystickIsXbox(1);
  }

  private Command m_autonomousCommand;

  private RobotContainer m_robotContainer;

  // public static final XboxController driverController = new XboxController(Constants.DRIVER_CONTROLLER_PORT);
  // public static final XboxController operatorController = new XboxController(Constants.OPERATOR_CONTROLLER_PORT);
  public static final GenericHID driverJoystick = new GenericHID(Constants.DRIVER_JOYSTICK_PORT);
  public static final GenericHID operatorJoystick = new Joystick(Constants.OPERATOR_JOYSTICK_PORT);
  public static final GenericHID danceDanceRevolution = new GenericHID(Constants.DANCE_DANCE_REVOLUTION_PORT);
  PowerDistribution pdh = new PowerDistribution();
  @Override
  public void robotInit() {
    SmartDashboard.putData("PDH", pdh);
    if (DriverStation.getJoystickIsXbox(Constants.DRIVER_JOYSTICK_PORT)) {

    } else {
      
    }
    m_robotContainer = new RobotContainer();
    SmartDashboard.putData("Commands", CommandScheduler.getInstance());
    SmartDashboard.putNumber("WaitTime", 0);
  }

  @Override
  public void robotPeriodic() {
    CommandScheduler.getInstance().run();
    SmartDashboard.putNumber("WheelRPMS", kDefaultPeriod);
    
  }

  @Override
  public void disabledInit() {
    
  }

  @Override
  public void disabledPeriodic() {}

  @Override
  public void disabledExit() {}

  @Override
  public void autonomousInit() {
    m_autonomousCommand = m_robotContainer.getAutonomousCommand();

    if (m_autonomousCommand != null) {
      m_autonomousCommand.schedule();
    }
  }

  @Override
  public void autonomousPeriodic() {}

  @Override
  public void autonomousExit() {}

  @Override
  public void teleopInit() {
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
  }

  @Override
  public void teleopPeriodic() {}

  @Override
  public void teleopExit() {}

  @Override
  public void testInit() {
    CommandScheduler.getInstance().cancelAll();
  }

  @Override
  public void testPeriodic() {}

  @Override
  public void testExit() {}
}
